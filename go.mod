module gitlab.com/digitaldollar/bepswap/mock-binance

go 1.13

replace gitlab.com/digitaldollar/bepswap/mock-binance => ../mock-binance

require (
	github.com/binance-chain/go-sdk v1.1.3
	github.com/gin-gonic/gin v1.4.0
	github.com/pkg/errors v0.8.1
	github.com/stumble/gorocksdb v0.0.3 // indirect
	github.com/tendermint/btcd v0.1.1 // indirect
	github.com/tendermint/go-amino v0.15.0
	github.com/tendermint/tendermint v0.32.7 // indirect
	github.com/ugorji/go v1.1.7 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127
)
